import { Todo } from 'src/app/models/Todo';

function generateTodos(count: number = 2): Todo[] {
    const result = [];
    for (let i = 1; i <= count; i++) {
        result.push({
            id: i.toString(),
            name: 'My todo ' + i,
            description: 'My todo description ' + i,
            startDate: new Date(2020, i, 10).getTime(),
            endDate: new Date(2020, i + 2, 10).getTime()
        } as Todo);
    }
    return result;
}

export const MOCK_TODOS: Todo[] = generateTodos();
