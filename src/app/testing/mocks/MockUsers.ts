import { User } from 'src/app/models/User';

export const MOCK_USERS: User[] = [
    {
        id: '1234',
        firstName: 'John',
        lastName: 'Doe',
        role: 'CEO'
    },
    {
        id: '5678',
        firstName: 'Jane',
        lastName: 'Dane',
        role: 'CTO'
    }
];
