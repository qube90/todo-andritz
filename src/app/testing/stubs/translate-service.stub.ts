import { EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';

export class TranslateServiceStub {
	readonly MOCK_TRANSLATIONS: { [key: string]: any } = {

	};

	public onLangChange: EventEmitter<any> = new EventEmitter();
	public onTranslationChange: EventEmitter<any> = new EventEmitter();
	public onDefaultLangChange: EventEmitter<any> = new EventEmitter();

	get(key: string): Observable<string | any> {
		return of(this._get(key, this.MOCK_TRANSLATIONS));
	}

	private _get(key: string, translation?: any): any {
		const keys = key.split('.');
		const next = translation[keys[0]];
		if (keys.length === 1) {
			return next || keys.join('.');
		}
		return this._get(keys.slice(1).join('.'), next);
	}
}
