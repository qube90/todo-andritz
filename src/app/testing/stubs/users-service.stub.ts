import { Observable, of } from 'rxjs';
import { MOCK_USERS } from '../mocks/MockUsers';
import { User } from 'src/app/models/User';

export class UsersServiceStub {
    getUsers(): Observable<User[]> {
        return of(MOCK_USERS);
    }
}
