import { of, Observable } from 'rxjs';
import { MOCK_TODOS } from '../mocks/MockTodos';
import { Todo } from 'src/app/models/Todo';

export class TodosServiceStub {
    getTodos(): Observable<Todo[]> {
        return of(MOCK_TODOS);
    }

    addTodo(todo: Todo): Promise<any> {
        return new Promise(resolve => setTimeout(resolve, 1000));
    }

    updateTodo(id: string, todo: Todo): Promise<any> {
        return new Promise(resolve => setTimeout(resolve, 1000));
    }

    deleteTodo(id: string): Promise<any> {
        return new Promise(resolve => setTimeout(resolve, 1000));
    }
}
