import { NgModule, LOCALE_ID } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateServiceStub } from '../stubs/translate-service.stub';
import { NzBundleModule } from 'src/app/shared/nz-bundle.module';

@NgModule({
	imports: [
		TranslateModule.forRoot({ defaultLanguage: 'en' })
	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule,
		HttpClientTestingModule,
		BrowserAnimationsModule,
		RouterTestingModule,
		NzBundleModule
	],
	providers: [
		{
			provide: TranslateService,
			useClass: TranslateServiceStub,
		}
	],
})
export class SharedTestingModule { }
