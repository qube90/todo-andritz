import { trigger, transition, style, query, group, animate } from '@angular/animations';

export const fadeIn =
    trigger('routeAnimations', [
        transition('TodoListPage <=> TodoPage', [
            query(':enter, :leave', style({ position: 'absolute', width: 'calc(100% - 48px)', opacity: 1 })),
            group([
                query(':enter', [
                    style({ opacity: 0 }),
                    animate('0.4s', style({ opacity: 1 }))
                ]),
                query(':leave', [
                    style({ opacity: 1 }),
                    animate('0.4s', style({ opacity: 0 }))]),
            ])
        ])
    ]);
