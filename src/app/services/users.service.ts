import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { User } from '../models/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private firestore: AngularFirestore) { }

  getUsers(): Observable<User[]> {
    return this.firestore.collection<User>('users').snapshotChanges().pipe(map((users) => {
      console.log(users);
      return users.map((u) => {
        const data = u.payload.doc.data() as User;
        const id = u.payload.doc.id;
        return { id, ...data, ref: u.payload.doc.ref };
      });
    }));
  }
}
