import { TestBed } from '@angular/core/testing';

import { TodosService } from './todos.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MOCK_TODOS } from '../testing/mocks/MockTodos';
import { of } from 'rxjs';

const MOCK_TODOS_COLLECTION = MOCK_TODOS.map(todo => {
  return {
    payload: {
      doc: {
        id: todo.id,
        data: () => todo
      }
    }
  }
})

describe('TodosService', () => {
  let service: TodosService;

  const data = of(MOCK_TODOS_COLLECTION);

  const collectionStub = {
    snapshotChanges: jasmine.createSpy('snapshotChanges').and.returnValue(data)
  }

  const angularFirestoreStub = {
    collection: jasmine.createSpy('collection').and.returnValue(collectionStub)
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TodosService,
        { provide: AngularFirestore, useValue: angularFirestoreStub }
      ]
    });

    service = TestBed.inject(TodosService);

  });

  it('should create service successfully', () => {
    expect(service).toBeTruthy();
  });

  it('should test if getTodos return correct value', (done: DoneFn) => {
    service.getTodos().subscribe(todos => {
      expect(todos.length).toBe(2);
      expect(todos[0].description).toEqual(MOCK_TODOS[0].description);
      expect(todos[0].startDate).toEqual(MOCK_TODOS[0].startDate);
      expect(todos[0].endDate).toEqual(MOCK_TODOS[0].endDate);
      done();
    })
  });

  it('should test private method _getStatus', () => {
    const now = Date.now();

    let status = service['_getStatus'](now - 10, now + 20);
    expect(status).toBeFalse();

    status = service['_getStatus'](now + 10, now - 10);
    expect(status).toBeTrue();

    status = service['_getStatus'](now + 10, now + 10);
    expect(status).toBeNull();
  });


});
