import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private fAuth: AngularFireAuth) { }

  signIn({ email, password }: { email: string; password: string }): Promise<any> {
    // Firebase docs:
    // https://firebase.google.com/docs/reference/js/firebase.auth.Auth#signinwithemailandpassword
    return this.fAuth.signInWithEmailAndPassword(email, password).catch(err => {
      if (err?.code.includes('auth')) {
        throw { status: 400 };
      }
      throw err;
    });
  }

  signOut(): Promise<any> {
    return this.fAuth.signOut();
  }
}
