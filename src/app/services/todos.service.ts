import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  QueryFn
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Todo } from '../models/Todo';

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  private collection: AngularFirestoreCollection<Todo>;

  private queryFilter: QueryFn = ref => {
    const now = Date.now();
    return ref.where('startDate', '<=', now) && ref.where('endDate', '>', now);
  }

  constructor(private firestore: AngularFirestore) {
    this.collection = this.firestore.collection<Todo>('todos');
  }

  getTodos(): Observable<Todo[]> {
    return this.collection.snapshotChanges()
      .pipe(
        map((todos) => this._prepareTodos(todos))
      );
  }

  getCurrentTodos(): Observable<Todo[]> {
    return this.firestore.collection<Todo>('todos', this.queryFilter)
      .snapshotChanges()
      .pipe(
        map((todos) => this._prepareTodos(todos))
      );
  }

  addTodo(todo: Todo): Promise<any> {
    const id = this.firestore.createId();
    return this.collection.doc(id).set(todo);
  }

  updateTodo(id: string, todo: Todo): Promise<any> {
    return this.collection.doc(id).update(todo);
  }

  deleteTodo(id: string): Promise<any> {
    return this.collection.doc(id).delete();
  }

  private _prepareTodos(todos: any[]): Todo[] {
    return todos.map(t => {
      const data = t.payload.doc.data() as Todo;
      const id = t.payload.doc.id;
      const status = this._getStatus(data.startDate, data.endDate);
      return { id, ...data, status };
    })
  }

  private _getStatus(startDate: number, endDate: number): boolean | null {
    const now = Date.now();
    if (now >= startDate && now < endDate) {
      // In progress
      return false;
    } else if (now > endDate) {
      // Finished
      return true;
    } else {
      // Not started
      return null;
    }
  }

}
