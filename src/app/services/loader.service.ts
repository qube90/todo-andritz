import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private counter = new BehaviorSubject<number>(0);

  get count(): number {
    return this.counter.getValue();
  }

  get loading(): boolean {
    return this.counter.getValue() > 0;
  }

  constructor() { }

  add() {
    this.counter.next(this.count + 1);
  }

  remove() {
    this.counter.next(this.count - 1);
  }
}
