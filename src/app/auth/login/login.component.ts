import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  visible = false;

  loading = false;

  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.loginForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, Validators.required]
    }, { updateOn: 'blur' });
  }

  signIn(): void {

    if (this.loginForm.invalid) {
      this.validate();
      return;
    }

    this.loading = true;

    this.authService.signIn(this.loginForm.value)
      .then(() => {
        this.router.navigateByUrl('/dashboard');
      })
      .catch(err => {
        if (err.status === 400) {
          this.validate();
        }
      })
      .finally(() => this.loading = false);
  }

  validate(): void {
    Object.keys(this.loginForm.controls).forEach(k => {
      this.loginForm.controls[k].markAsDirty();
      this.loginForm.controls[k].updateValueAndValidity();
    });
  }
}
