import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router, RouterOutlet } from '@angular/router';
import { fadeIn } from '../animations/fade-in';

interface NavItem {
  href: string;
  label: string;
  icon: string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [fadeIn]
})
export class MainComponent {
  isCollapsed = false;

  navItems: NavItem[] = [
    {
      href: '/dashboard',
      label: 'DASHBOARD',
      icon: 'dashboard'
    },
    {
      href: '/todos',
      label: 'TODOS',
      icon: 'unordered-list'
    }
  ];

  constructor(private authService: AuthService, private router: Router) { }

  signOut(): void {
    this.authService.signOut().then(() => {
      this.router.navigateByUrl('/auth');
    });
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

}
