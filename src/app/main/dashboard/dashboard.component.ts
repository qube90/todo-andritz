import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TodosService } from 'src/app/services/todos.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  usersCount$: Observable<number>;

  currentTodosCount$: Observable<number>;

  constructor(private usersService: UsersService, private todosService: TodosService) { }

  ngOnInit(): void {
    this.usersCount$ = this.usersService.getUsers()
      .pipe(
        map(users => users.length)
      );

    this.currentTodosCount$ = this.todosService.getCurrentTodos()
      .pipe(
        map(todos => todos.length)
      );
  }

}
