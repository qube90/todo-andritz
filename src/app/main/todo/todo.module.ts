import { NgModule } from '@angular/core';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
  { path: '', component: TodosComponent, data: { animation: 'TodoListPage' } },
  { path: 'add', component: TodoComponent, data: { animation: 'TodoPage' } },
  { path: 'edit/:id', component: TodoComponent, data: { edit: true, animation: 'TodoPage' } },
];

@NgModule({
  declarations: [TodosComponent, TodoComponent],
  imports: [SharedModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoModule { }
