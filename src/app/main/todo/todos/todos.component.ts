import { Component, OnInit } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { Observable } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
})
export class TodosComponent implements OnInit {
  todos$: Observable<any[]>;

  constructor(private todosService: TodosService, public loader: LoaderService) { }

  ngOnInit(): void {
    this.todos$ = this.todosService.getTodos();
  }
}
