import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosComponent } from './todos.component';
import { TodosService } from 'src/app/services/todos.service';
import { TodosServiceStub } from 'src/app/testing/stubs/todos-service.stub';
import { SharedTestingModule } from 'src/app/testing/shared/shared-testing.module';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;

  const getTodoRows = (): DebugElement[] => {
    return fixture.debugElement.queryAll(By.css('tbody > tr'));
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TodosComponent],
      imports: [SharedTestingModule],
      providers: [
        { provide: TodosService, useClass: TodosServiceStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component successfully', () => {
    expect(component).toBeTruthy();
  });

  it('should render 2 table rows', () => {
    const rows = getTodoRows();
    expect(rows.length).toBe(2);
  });



});
