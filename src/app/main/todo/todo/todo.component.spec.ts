import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TodoComponent } from './todo.component';
import { TodosService } from 'src/app/services/todos.service';
import { TodosServiceStub } from 'src/app/testing/stubs/todos-service.stub';
import { UsersService } from 'src/app/services/users.service';
import { UsersServiceStub } from 'src/app/testing/stubs/users-service.stub';
import { SharedTestingModule } from 'src/app/testing/shared/shared-testing.module';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { click } from 'src/app/testing/utils';

describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  const getSubmitButton = (): DebugElement => {
    return fixture.debugElement.query(By.css('button[type=submit]'));
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TodoComponent],
      imports: [SharedTestingModule],
      providers: [
        { provide: TodosService, useClass: TodosServiceStub },
        { provide: UsersService, useClass: UsersServiceStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component successfully', () => {
    expect(component).toBeTruthy();
  });


  describe('Test private methods', () => {
    it('_getPaylod private method should return specific value', () => {
      const payload = component['_getPaylod']({
        name: 'Test',
        description: 'Test description',
        range: [new Date(2020, 8, 12), new Date(2020, 9, 12)]
      });

      expect(payload).toEqual({
        name: 'Test',
        description: 'Test description',
        startDate: 1599861600000,
        endDate: 1602453600000
      });
    });

    it('_getTimespan private method should return specific value', () => {
      const timespan = component['_getTimespan']([new Date(1595455200000), new Date(1592604000000)]);

      expect(timespan).toEqual({
        startDate: 1595455200000,
        endDate: 1592604000000
      });
    });
  });

  describe('Test create mode', () => {

    it('todoId should NOT be defined', () => {
      expect(component.todoId).toBeUndefined();
    });

    it('should test if form is initially invalid and submit button is disabled', () => {
      expect(component.todoForm.valid).toBeFalse();
      const submitButton = getSubmitButton();
      expect(submitButton.attributes.disabled).toBe('true');
    });

    it('should patch todoForm with mock value and check if form is valid', () => {
      component.todoForm.patchValue({
        name: 'Todo',
        description: 'Todo description',
        range: [new Date(2020, 5, 10), new Date(2020, 9, 12)],
        userId: '1234'
      });

      fixture.detectChanges();

      expect(component.todoForm.valid).toBeTrue();
    });

    it('should click on submit button, check if loading is true, if submit button is disabled, wait for 1s and check if loading is back to false and submit button is NOT disabled', fakeAsync(() => {
      const spy = spyOn(component, 'addTodo');

      component.todoForm.patchValue({
        name: 'Todo',
        description: 'Todo description',
        range: [new Date(2020, 5, 10), new Date(2020, 9, 12)],
        userId: '1234'
      });

      fixture.detectChanges();

      let submitButton = getSubmitButton();

      click(submitButton.nativeElement);

      fixture.detectChanges();

      expect(spy).toHaveBeenCalled();

      tick(1000);

      submitButton = getSubmitButton();

      expect(component.loading).toBeFalse();
      expect(submitButton.attributes.disabled).toBeUndefined();

    }));

  });

  describe('Test update mode', () => {

    beforeEach(() => {
      component.todoId = '1234';

      component.todoForm.patchValue({
        name: 'Todo',
        description: 'Todo description',
        range: [new Date(2020, 5, 10), new Date(2020, 9, 12)],
        userId: '1234'
      });

      fixture.detectChanges();
    });

    it('todoId should be defined', () => {
      expect(component.todoId).toBeDefined();
    });

    it('todoForm should be valid', () => {
      expect(component.todoForm.valid).toBeTrue();
    });

  });

});
