import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TodosService } from 'src/app/services/todos.service';
import { UsersService } from 'src/app/services/users.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/User';
import { Todo } from 'src/app/models/Todo';
import { tap, switchMapTo, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  todoId: string;

  get isEdit(): boolean {
    return !!this.todoId;
  }

  todoForm: FormGroup;

  get name(): AbstractControl {
    return this.todoForm.get('name');
  }

  users$: Observable<User[]>;

  selectedUser: User;

  loading = false;

  deleting = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private todosService: TodosService,
    private usersService: UsersService,
    private router: Router
  ) {
    this.todoForm = this.fb.group({
      name: [null, [Validators.required, Validators.maxLength(100)]],
      description: [null, [Validators.required, Validators.maxLength(4000)]],
      range: [[], Validators.required],
      userId: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      // Get id from url
      map(paramMap => paramMap.get('id')),
      filter(id => !!id),
      tap(id => this.todoId = id),
      // Get to-dos if id present
      switchMapTo(this.todosService.getTodos()),
      // Find to-do based on the id
      map(todos => {
        return todos.find(({ id }) => this.todoId === id);
      }),
      filter(todo => !!todo)
    ).subscribe(todo => {
      this.todoForm.patchValue({
        name: todo.name,
        description: todo.description,
        range: [new Date(todo.startDate), new Date(todo.endDate)],
        userId: todo.user.id
      });
    });

    this.users$ = this.usersService.getUsers().pipe(tap(users => {
      console.log(users);
      this.todoForm.get('userId').valueChanges.subscribe(userId => {
        this.selectedUser = users.find(user => user.id === userId);
      })
    }));

  }

  addTodo() {
    this.loading = true;
    this.todosService.addTodo(this._getPaylod(this.todoForm.value))
      .then(() => this.todoForm.reset())
      .finally(() => this.loading = false);
  }

  updateTodo() {
    this.loading = true;
    this.todosService.updateTodo(this.todoId, this._getPaylod(this.todoForm.value))
      .finally(() => this.loading = false);
  }

  deleteTodo() {
    this.deleting = true;
    this.todosService.deleteTodo(this.todoId)
      .then(() => this.router.navigateByUrl('/todos'))
      .finally(() => this.deleting = false);
  }

  private _getPaylod(formValue: { name: string; description: string; range: Date[] }): Todo {
    const payload: Todo = {
      name: formValue.name,
      description: formValue.description,
      user: this.selectedUser,
      ...this._getTimespan(formValue.range)
    };

    delete (payload as any).range;

    return payload;
  }

  private _getTimespan(range: Date[]): { startDate: number; endDate: number } {
    const [start, end] = range;
    return {
      startDate: start.getTime(),
      endDate: end.getTime()
    };
  }

}
