import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzSpinModule } from 'ng-zorro-antd/spin';

import { enUS } from 'date-fns/locale';
import { NZ_DATE_LOCALE } from 'ng-zorro-antd/i18n';

@NgModule({
  exports: [
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzListModule,
    NzCardModule,
    NzInputModule,
    NzFormModule,
    NzEmptyModule,
    NzSkeletonModule,
    NzGridModule,
    NzDatePickerModule,
    NzSelectModule,
    NzTableModule,
    NzStatisticModule,
    NzPopconfirmModule,
    NzToolTipModule,
    NzSpinModule
  ],
  providers: [
    { provide: NZ_DATE_LOCALE, useValue: enUS }
  ]
})
export class NzBundleModule { }
