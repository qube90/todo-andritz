import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzBundleModule } from './nz-bundle.module';
import { TranslateModule } from '@ngx-translate/core';
import { IconsProviderModule } from './icons-provider.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    IconsProviderModule,
    NzBundleModule,
  ],
})
export class SharedModule { }
