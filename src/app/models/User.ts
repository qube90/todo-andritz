import { DocumentReference } from '@angular/fire/firestore';

export interface User {
    id: string;
    firstName: string;
    lastName: string;
    role: string;
}
