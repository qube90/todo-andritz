import { User } from './User';

export interface Todo {
    id?: string;
    name: string;
    description: string;
    endDate: number;
    startDate: number;
    user?: User;
    status?: boolean | null;
}
